﻿using System;

namespace Logger
{
	public class ConsoleLogBackend : ILogBackend
	{
		private LogLevel logMask;

		public LogLevel LogMask
		{
			get { return logMask; }
			set { logMask = value; }
		}

		public ConsoleLogBackend () 
		{
			this.LogMask = LogLevel.ALL;
		}

		public ConsoleLogBackend (LogLevel level)
		{
			this.logMask = level;
		}

		public void Log(string message)
		{
			Console.WriteLine ("[" + DateTime.Now.ToString ("dd-MM-yy HH:mm:ss") + "] " + message + " Level: " + LogMask);
		}

	}
}

