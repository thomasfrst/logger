﻿using System;

namespace Logger
{
	// Backand for logger
	public interface ILogBackend
	{

		// Mask for types of messages we want to log
		LogLevel LogMask { get; set; }

		void Log(string message);

	}
}

