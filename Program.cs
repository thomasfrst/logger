﻿using System;

namespace Logger
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			DefaultLogger logger = new DefaultLogger ();

			ConsoleLogBackend defaultBackend = new ConsoleLogBackend ();
			ConsoleLogBackend infoBackend = new ConsoleLogBackend (LogLevel.INFO);
			ConsoleLogBackend warningBackend = new ConsoleLogBackend (LogLevel.WARNING);
			ConsoleLogBackend errorBackend = new ConsoleLogBackend (LogLevel.ERROR);


			logger.AddAllBackends (defaultBackend, infoBackend, warningBackend, errorBackend);

			logger.LogMessage ("Info at {0}", LogLevel.INFO, (object)DateTime.Now.Hour);
			logger.LogMessage ("Warning at {0}", LogLevel.WARNING, (object)DateTime.Now.Hour);
			logger.LogMessage ("Error at {0}", LogLevel.ERROR, (object)DateTime.Now.Hour);
		}
	}
}
