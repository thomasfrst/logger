﻿using System;

namespace Logger
{
	public enum LogLevel : int
	{
		INFO = 1 << 0,
		WARNING = 1 << 1,
		ERROR = 1 << 2,
		ALL = INFO | WARNING | ERROR
	}
}

