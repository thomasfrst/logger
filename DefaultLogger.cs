﻿using System;

namespace Logger
{
	public class DefaultLogger : LoggerBase
	{
		public override void LogMessage(string text, LogLevel level, params object[] data)
		{
			string res = string.Format (text, data);
			foreach (ILogBackend backend in backends)
			{
				// If we want this backend
				if (((int) backend.LogMask & (int) level) != 0)
				{
					backend.Log (res);
				}
			}
		}

		public override void AddBackend (ILogBackend backend)
		{
			backends.Add (backend);
		}
	}
}

