﻿using System;
using System.Collections.Generic;

namespace Logger
{
	public abstract class LoggerBase
	{
		public List<ILogBackend> backends;

		public LoggerBase() { backends = new List<ILogBackend> (); }

		public abstract void LogMessage(string text, LogLevel level, params object[] data);

		public abstract void AddBackend (ILogBackend backend);

		public virtual void AddAllBackends (params ILogBackend[] backends)
		{
			foreach (ILogBackend backend in backends)
			{
				AddBackend (backend);
			}
		}
	}
}

